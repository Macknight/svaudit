
/**
BLUEPRINT
**/
INSERT INTO `tb_audit`.`t_audit_blueprint` (`PK_BLUEPRINT_ID`, `C_NAME`) VALUES ('1', 'Leadership Safety Audit - Manufactoring');
/**
ITENS
**/
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('1', '5S - HOUSEKEEPING', 'The work enviroment is clean and organized (SES - 1.8.19)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('2', '5S - HOUSEKEEPING', 'There are no materials on the floor (SES - 1.8.19)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('3', '5S - HOUSEKEEPING', 'The floor area is correctly demarcated.', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('4', '5S - HOUSEKEEPING', 'The ceiling is clean without any kind of dirt.', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('5', '5S - HOUSEKEEPING', 'There  are pallets left in a circulation area (SES 1.5.15)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('6', 'PPE\'s', 'The PPEs are adequated to the task (SES - 1.2.3)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('7', 'PPE\'s', 'The PPE\'S are correctly allocated', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('8', 'ELECTRICITY', 'All electrical panels are identified and protected', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('9', 'ELECTRICITY', 'There no damaes in the cables (SES - 2.4.1)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('10', 'EQUIPMENTS AND MACHINES ', 'There are no equipment with exposed moving parts (SES - 1.3.2)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('11', 'EQUIPMENTS AND MACHINES ', 'There are no damages in the cables (SES - 1.3.3)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('12', 'TOOLS', 'All the tools are in perfect condition', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('13', 'TOOLS', 'The electrical tool\'s cables and sockets are in good condition (SES 2-4)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('14', 'TOOLS', 'The esmeril has the cover installed', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('15', 'FIRE PREVENTIONS ', 'The escape route is available in all shop long (F-PES 6.5.1)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('16', 'FIRE PREVENTIONS ', 'The fire prevent equipments are in perfect use condition  (F-PES 6.1.3 )', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('17', 'FIRE PREVENTIONS ', 'The building brigadist are correctly identified (F-PES - )', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('18', 'FIRE PREVENTIONS ', 'There are emergency lights in the shop and well functioning  (F-PES)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('19', 'FIRE PREVENTIONS ', 'There are no flammable material out of the fireproof cabinet  (F-PES 5.3)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('20', 'HANDLING MATERIALS - LOGISTIC', 'The safety Check list of Forklift is performed daily', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('21', 'HANDLING MATERIALS - LOGISTIC', 'The driver uses the handling brake when leaving the vehicle', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('22', 'HANDLING MATERIALS - LOGISTIC', 'The load is well distributed and centralized on the fork', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('23', 'HANDLING MATERIALS - LOGISTIC', 'The driver uses the seat belt fastened', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('24', 'HANDLING MATERIALS - LOGISTIC', 'There are operators transporting loads in the stardard high levels', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('25', 'CHEMICALS & FLAMMABLE PRODUCTS', 'There is any hot work or spark work on site containing flammable materials', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('26', 'CHEMICALS & FLAMMABLE PRODUCTS', 'The chemicals products are correctly identified', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('27', 'RISK ASSESSMENT/ BEHAVIOR', 'There is any unsafe act being committed during the observed activity', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('28', 'RISK ASSESSMENT/ BEHAVIOR', 'There are advising signals that alerts about the risks', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('29', 'RISK ASSESSMENT/ BEHAVIOR', 'The Yoshi-Yoshi rile is been done', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('30', 'ENVIROMENT', 'The selective colleciton is implemented.', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('31', 'ENVIROMENT', 'Waste collector is appropriated (color/identification)', '1');
INSERT INTO `tb_audit`.`t_audit_item` (`PK_AUDIT_ITEM_ID`, `C_AUDIT_ITEM_CATEGORY`, `C_QUESTION`, `C_ATIVO`) VALUES ('32', 'ENVIROMENT', 'There is any place that favor the proliferation of vectors', '1');
UPDATE `tb_audit`.`t_audit_item` SET `C_QUESTION`='There are advising signals that alerts about the risks _(SES 1.3.4)' WHERE `PK_AUDIT_ITEM_ID`='28';
UPDATE `tb_audit`.`t_audit_item` SET `C_QUESTION`='There is any unsafe act being committed during the observed activity (SES 1.4.1)' WHERE `PK_AUDIT_ITEM_ID`='27';
UPDATE `tb_audit`.`t_audit_item` SET `C_QUESTION`='The chemicals products are correctly identified (F-PES 4.1)' WHERE `PK_AUDIT_ITEM_ID`='26';
UPDATE `tb_audit`.`t_audit_item` SET `C_QUESTION`='There is any hot work or spark work on site containing flammable materials (F-PES 4.1)' WHERE `PK_AUDIT_ITEM_ID`='25';
UPDATE `tb_audit`.`t_audit_item` SET `C_QUESTION`='There are operators transporting loads in the stardard high levels (SES -1.5.3)' WHERE `PK_AUDIT_ITEM_ID`='24';
UPDATE `tb_audit`.`t_audit_item` SET `C_QUESTION`='The driver uses the seat belt fastened ' WHERE `PK_AUDIT_ITEM_ID`='23';
UPDATE `tb_audit`.`t_audit_item` SET `C_QUESTION`='The load is well distributed and centralized on the fork (SES -1.5.3)' WHERE `PK_AUDIT_ITEM_ID`='22';
UPDATE `tb_audit`.`t_audit_item` SET `C_QUESTION`='The driver uses the handling brake when leaving the vehicle (SES -1.5.4)' WHERE `PK_AUDIT_ITEM_ID`='21';


/*
blueprint itens MAP
*/


INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '1', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '2', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '3', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '4', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '5', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '6', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '7', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '8', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '9', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '10', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '11', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '12', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '13', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '14', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '15', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '16', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '17', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '18', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '19', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '20', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '21', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '22', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '23', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '24', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '25', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '26', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '27', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '28', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '29', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '30', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '31', '1');
INSERT INTO `tb_audit`.`t_blueprint_itens` (`FK_BLUEPRINT_ID`, `FK_AUDIT_ITEM_ID`, `B_MANDATORYT`) VALUES ('1', '32', '1');
