package br.com.svconsultoria.Model;

public class User {
	
	int id;
	String name;
	String password;
	String category;
	String login;
	boolean ativo;
	
	public static final int USER_ADMIN = 1;
	public static final int USER_AUDITOR = 2;
	
	
	public User(int id, String name, String password, String category, String login) {
		super();
		this.id = id;
		this.name = name;
		this.password = password;
		this.category = category;
		this.login = login;
	}
	public User( String name, String password, String category, String login, boolean ativo) {
		super();
	
		this.name = name;
		this.password = password;
		this.category = category;
		this.login = login;
		this.ativo = ativo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public boolean getAtivo() {
		return this.ativo;
	}
	
	

	

	
}
