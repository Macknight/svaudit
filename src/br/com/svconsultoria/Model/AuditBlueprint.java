package br.com.svconsultoria.Model;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import br.com.svconsultoria.DAO.AuditItensDao;
import br.com.svconsultoria.Model.AuditItem;

public class AuditBlueprint {
	int id;
	String name;
	List<AuditItem> itens;
	
	public AuditBlueprint(int id, String name, List<AuditItem> itens) {
		super();
		this.id = id;
		this.name = name;
		this.itens = itens;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	public List<AuditItem> getItens() {
		if(itens!=null) {
			return itens;
		}else {
			itens = new ArrayList<AuditItem>();
			return itens;
		}

	}

	public void setItens(List<AuditItem> itens) {
		this.itens = itens;
	}

	public AuditBlueprint(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	
	public AuditBlueprint() {
		this.name = "placeholder";
		this.itens = new ArrayList<AuditItem>();
	}

	public void populateItens(DataSource dataSource) {
		AuditItensDao.populateBlueprint(this, dataSource);
	}

	public void addItem(AuditItem insertionItem) {
		// TODO Auto-generated method stub
		if(this.itens!=null) {
			itens.add(insertionItem);
		}else {
			itens = new ArrayList<AuditItem>();
			itens.add(insertionItem);
		}
	}
}
