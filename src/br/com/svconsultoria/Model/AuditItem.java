package br.com.svconsultoria.Model;

public class AuditItem {
	String question;
	int id;
	String category;
	boolean mandatory;
	
	public AuditItem( int id,String question, String category, boolean mandatory) {
		super();
		this.question = question;
		this.id = id;
		this.category = category;
		this.mandatory = mandatory;
	}

	public AuditItem( int id,String question, String category) {
		super();
		this.question = question;
		this.id = id;
		this.category = category;
	
	}
	
	
	public AuditItem(String itemDescription, String category) {
		this.question = itemDescription;
		
		this.category = category;
		this.mandatory = true;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public boolean getMandatory() {
		return this.mandatory;
	}
	
}
