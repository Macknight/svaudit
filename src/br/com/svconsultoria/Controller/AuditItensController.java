package br.com.svconsultoria.Controller;

import java.io.IOException;
import java.util.List;
import java.util.TreeSet;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import br.com.svconsultoria.DAO.AuditItensDao;
import br.com.svconsultoria.Model.AuditItem;


@WebServlet("/AuditItensController")
public class AuditItensController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Resource(name = "jdbc/SVAuditDB")
	private DataSource dataSource;
	
	RequestDispatcher dispatcher;
   
    public AuditItensController() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String command = (String) request.getParameter("command");
		switch (command) {
		case "show":
			
			showItens(request,response);
			break;
		case "add":
			//gets parameter from modal and adds new item
			addItem(request,response); 
			showItens(request,response);
			break;
		case "delete":
			//gets parameter from modal and adds new item
			deleteItem(request,response); 
			showItens(request,response);
			break;
		case "edit":
			//gets parameter from modal and adds new item
			updateItem(request,response); 
			showItens(request,response);
			break;

		default:
			
			showItens(request,response);
			break;
		}
	
		
	}


	private void updateItem(HttpServletRequest request, HttpServletResponse response) {
		
		
		int itemId = Integer.valueOf(request.getParameter("itemID"));
	
		
		AuditItem newAuditItem;
		String itemDescription = request.getParameter("itemDescription");
		
		String newCategory = request.getParameter("newCategory");
				
		String category = request.getParameter("categoryId");
		if(newCategory==null || newCategory.equals("")) {
			 newAuditItem = new AuditItem(itemId, itemDescription, category);
		}else {
			newAuditItem = new AuditItem(itemId, itemDescription, newCategory);
		}
		
		
		
	
		AuditItensDao.updateItem(newAuditItem, dataSource);
		
	}


	private void deleteItem(HttpServletRequest request, HttpServletResponse response) {
		int itemId = Integer.valueOf(request.getParameter("deleteId"));
		AuditItensDao.deleteItem(itemId, dataSource);
	}


	private void addItem(HttpServletRequest request, HttpServletResponse response) {
		AuditItem newAuditItem;
		String itemDescription = request.getParameter("itemDescription");
		
		String newCategory = request.getParameter("newCategory");
				
		String category = request.getParameter("categoryId");
		if(newCategory==null || newCategory.equals("")) {
			 newAuditItem = new AuditItem( itemDescription, category);
		}else {
			newAuditItem = new AuditItem(itemDescription, newCategory);
		}
		
		AuditItensDao.insertNewAuditItem(newAuditItem, dataSource);
		
		
		
	}


	private void showItens(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//getting all items
		List<AuditItem> itens = AuditItensDao.getAllActiveAuditItens(dataSource);
		TreeSet<String> categories = new TreeSet<String>();
		//get categories from the itens list
		
		for (AuditItem item : itens) {
			categories.add(item.getCategory());
		}
		request.setAttribute("categories", categories);
		request.setAttribute("itens", itens);
		
	
		dispatcher = request.getRequestDispatcher("/itens.jsp");
		dispatcher.forward(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
