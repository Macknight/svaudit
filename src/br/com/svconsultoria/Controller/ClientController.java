package br.com.svconsultoria.Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.svconsultoria.Utils.RequestUtils;
/*
 * 
 * 
 * PlaceHolder for future version of a cliente controller, which will switch between clients interfaces and users.
 * 
 */

@WebServlet("/ChooseClient")
public class ClientController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public ClientController() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		
		String URL = RequestUtils.getCurrentUrl(request);
		PrintWriter out = response.getWriter();
		out.println(URL);
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
