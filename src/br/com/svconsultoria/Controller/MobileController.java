package br.com.svconsultoria.Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.svconsultoria.Utils.RequestUtils;

/**
 * Servlet implementation class MobileController
 */
@WebServlet("/MobileController")
public class MobileController extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
    public MobileController() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		RequestUtils.printRequestParameters(request, this.getServletName());
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
		RequestUtils.printRequestParameters(request, this.getServletName());
	}

}
