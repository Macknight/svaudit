package br.com.svconsultoria.Controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ReportController
 */
@WebServlet("/ReportController")
public class ReportController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	//Two types of reports SUMMARIZED or ANALYTICAL
	private static final String SUMMARIZED = "summarized"; 
    private static final String ANALYTICAL= "analytical"; 
    //Report Objects
    private static final String USER= "user"; 
    private static final String BLUEPRINT= "blueprint"; 
    private static final String AUDITLOG= "auditLog"; 
    private static final String AUDITITEM= "auditItem"; 
    //controller constante
    private static final String SHOW= "show"; 
    
    
    
    
  
    public ReportController() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//gets the command
		String command = (String) request.getParameter("command");
		//gets the object the user wants a report		
		String object= (String) request.getParameter("reportObject");
		//gets the type (analitycal or summarized)
		String type = (String) request.getParameter("reportType");
		switch(command) {
		case SHOW:
			selectType(object,type);
		}
		
		
		
	}

	
	private void selectType(String object, String type) {
		if(type.equals(SUMMARIZED)) {
			showSummarizedReport(object);
		}else if(type.equals(ANALYTICAL)) {
			showAnalitycalReport(object);
		}
	}


	private void showAnalitycalReport(String object) {
		switch(object) {
		case USER:
			break;
			
		case BLUEPRINT:
			break;
			
		case AUDITLOG:
			break;
			
		case AUDITITEM:
			break;
			
		}
		
	}


	private void showSummarizedReport(String object) {
		switch(object) {
		case USER:
			break;
			
		case BLUEPRINT:
			break;
			
		case AUDITLOG:
			break;
			
		case AUDITITEM:
			break;
			
		}
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
