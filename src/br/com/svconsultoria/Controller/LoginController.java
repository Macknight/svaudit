package br.com.svconsultoria.Controller;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import br.com.svconsultoria.DAO.UserDao;
import br.com.svconsultoria.Model.User;

@WebServlet("/LoginCommand")
public class LoginController extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	//LOGIN STATUS
    public static final String USER_NOT_FOUND = "USER_NOT_FOUND";   
    public static final String PASSWORD_INCORRECT= "PASSWORD_INCORRECT";   
    public static final String LOGIN_RESPONSE = "LOGIN_RESPONSE";
    
   
   @Resource(name="jdbc/SVAuditDB")
   private DataSource dataSource;
   
   RequestDispatcher dispatcher;
   
   

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doLogin(request, response);
	
	}


	private void doLogin(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String login = request.getParameter("login");
		String password = request.getParameter("password");
	
		User user= UserDao.getUserByLogin(login,dataSource);
		if(user==null) {
			 request.setAttribute("LOGIN_RESPONSE",USER_NOT_FOUND );
			 dispatcher = request.getRequestDispatcher("/index.jsp");
			 dispatcher.forward(request, response);
		}else {
			if(user.getPassword().equals(password)) {
				dispatcher = request.getRequestDispatcher("/MainMenu.jsp");
				dispatcher.forward(request, response);
			}else {
				request.setAttribute("LOGIN_RESPONSE",PASSWORD_INCORRECT );
				dispatcher = request.getRequestDispatcher("/index.jsp");
				dispatcher.forward(request, response);
			}
		}
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	
}
