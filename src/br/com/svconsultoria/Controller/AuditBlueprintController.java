package br.com.svconsultoria.Controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeSet;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import br.com.svconsultoria.DAO.AuditBlueprintDao;
import br.com.svconsultoria.DAO.AuditItensDao;
import br.com.svconsultoria.Model.AuditBlueprint;
import br.com.svconsultoria.Model.AuditItem;


/**
 * Servlet implementation class AuditBlueprintController
 */
@WebServlet("/AuditBlueprintController")
public class AuditBlueprintController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@Resource(name = "jdbc/SVAuditDB")
	private DataSource dataSource;
	RequestDispatcher dispatcher;
	
    public AuditBlueprintController() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		String command = (String) request.getParameter("command");
		if(command==null) {
		 listAudits(request, response);
		 return;
		}
		switch (command) {
		case "show":
			listAudits(request,response);
			
			break;
		case "edit":
			
			showEditBlueprint(request,response);
			
			break;
		case "new":
			
			showNewBlueprint(request,response);
			
			break;
		case "insertItem":
			
			insertItemOnBlueprint(request,response);
			showUpdatedNewBlueprint(request,response);
		
			
			break;
		case "editItem":
			
			insertItemOnBlueprint(request,response);
			showUpdatedEditedBlueprint(request,response);
			
			
			break;
		case "saveBlueprint":
		
			saveBlueprint(request,response);
			listAudits(request, response);
			
			
			
			break;
	
		case "updateBlueprint":
			
			updateBlueprint(request,response);
			listAudits(request, response);
			
			
			
			break;
			

		default:
			break;
		}
	
	}

	
	private void updateBlueprint(HttpServletRequest request, HttpServletResponse response) {
		
		AuditBlueprint newBlueprint = (AuditBlueprint) request.getSession().getAttribute("newBlueprint");
		
		
		
		AuditBlueprintDao.updateBlueprintItens(newBlueprint,dataSource);
		
	}


	private void showUpdatedEditedBlueprint(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//iterate through items. Get the correct item from the list and remove
				@SuppressWarnings("unchecked")
				List<AuditItem> allAuditItens =  (List<AuditItem>) request.getSession().getAttribute("allAuditItens");
				TreeSet<String> categories = new TreeSet<String>();
			
				//running through itens, populating categories
				for (AuditItem item : allAuditItens) {
					categories.add(item.getCategory());
				}
			
				AuditBlueprint newBlueprint = (AuditBlueprint) request.getSession().getAttribute("newBlueprint");
				
				List<AuditItem> blueprintItens  = newBlueprint.getItens();
				TreeSet<String> blueprintsCategories = new TreeSet<String>();
				
				//running through itens, populating categories
				for (AuditItem item : blueprintItens) {
					blueprintsCategories.add(item.getCategory());
				}

				request.setAttribute("blueprintItens", blueprintItens);
				
				request.setAttribute("blueprintsCategories", blueprintsCategories);
				
				request.setAttribute("blueprint", newBlueprint);
				
				request.setAttribute("categories", categories);
				
				request.setAttribute("allAuditItens", allAuditItens);
				
				
				
				dispatcher = request.getRequestDispatcher("/editAuditBlueprint.jsp");
				dispatcher.forward(request, response);
		
	}


	private void saveBlueprint(HttpServletRequest request, HttpServletResponse response) {

		String blueprintName = request.getParameter("blueprintName");
		AuditBlueprint newBlueprint = (AuditBlueprint) request.getSession().getAttribute("newBlueprint");
		if(blueprintName!=null && !blueprintName.equals("")) {
			System.out.println(blueprintName);
			newBlueprint.setName(blueprintName);
		}
		
		
		AuditBlueprintDao.saveBlueprint(newBlueprint,dataSource);
	
		
	}


	private void showUpdatedNewBlueprint(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//iterate through items. Get the correct item from the list and remove
		@SuppressWarnings("unchecked")
		List<AuditItem> allAuditItens =  (List<AuditItem>) request.getSession().getAttribute("allAuditItens");
		TreeSet<String> categories = new TreeSet<String>();
	
		//running through itens, populating categories
		for (AuditItem item : allAuditItens) {
			categories.add(item.getCategory());
		}
	
		AuditBlueprint newBlueprint = (AuditBlueprint) request.getSession().getAttribute("newBlueprint");
		
		List<AuditItem> blueprintItens  = newBlueprint.getItens();
		TreeSet<String> blueprintsCategories = new TreeSet<String>();
		
		//running through itens, populating categories
		for (AuditItem item : blueprintItens) {
			blueprintsCategories.add(item.getCategory());
		}

		request.setAttribute("blueprintItens", blueprintItens);
		
		request.setAttribute("blueprintsCategories", blueprintsCategories);
		
		request.setAttribute("newBlueprint", newBlueprint);
		
		request.setAttribute("categories", categories);
		
		request.setAttribute("allAuditItens", allAuditItens);
		
		
		
		dispatcher = request.getRequestDispatcher("/NewAuditBlueprint.jsp");
		dispatcher.forward(request, response);
	}


	private void insertItemOnBlueprint(HttpServletRequest request, HttpServletResponse response) {
		//gets id from item to add.
		String id = request.getParameter("insertItemId");
		int questionId = Integer.valueOf(id);
		AuditItem insertionItem = null;
		
		//iterate through items. Get the correct item from the list and remove
		@SuppressWarnings("unchecked")
		List<AuditItem> allAuditItens =  (List<AuditItem>) request.getSession().getAttribute("allAuditItens");
		
		for (AuditItem auditItem : allAuditItens) {
			if(auditItem.getId()==questionId) {
				insertionItem = auditItem;
				break;
			}
		}
		System.out.println(insertionItem.getQuestion());
		allAuditItens.remove(insertionItem);
		//removes item from the item list (so the user can't repeat the same question)
		//initialize blueprint
		AuditBlueprint newBlueprint = (AuditBlueprint) request.getSession().getAttribute("newBlueprint");
		//get name from the adit
		String blueprintName = request.getParameter("blueprintName");
		if(blueprintName!=null && !blueprintName.equals("")) {
			newBlueprint.setName(blueprintName);
		}
		newBlueprint.addItem(insertionItem);
	}


	private void showNewBlueprint(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//get all itens to populate itens list in jsp
		List<AuditItem> allAuditItens = AuditItensDao.getAllAuditItens(dataSource);
		//makes a treeset of all categories.
		TreeSet<String> categories = new TreeSet<String>();
		//initialize blueprint
		AuditBlueprint newBlueprint = new AuditBlueprint();
		//running through itens, populating categories
		for (AuditItem item : allAuditItens) {
			categories.add(item.getCategory());
		}
		//treeset for all blueprint categories
		
	
		
		//putting objects on session so we can retrieve everything after inserting/deleting object
		request.getSession().setAttribute("newBlueprint", newBlueprint);
		request.getSession().setAttribute("categories", categories);
		request.getSession().setAttribute("allAuditItens", allAuditItens);
		
		
		request.setAttribute("newBlueprint", newBlueprint);
		request.setAttribute("categories", categories);
		request.setAttribute("allAuditItens", allAuditItens);
		
		
		
		dispatcher = request.getRequestDispatcher("/NewAuditBlueprint.jsp");
		dispatcher.forward(request, response);
		
	}


	private void showEditBlueprint(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		//get all items from db
		List<AuditItem> allAuditItens = AuditItensDao.getAllAuditItens(dataSource);
		
		
		
		//get id from jsp selected blueprint
		int editId = Integer.valueOf(request.getParameter("editID"));
		//get blueprint from id
		AuditBlueprint editBP = AuditBlueprintDao.getBlueprint(editId, dataSource);
		//populate itens for that blueprint
		editBP.populateItens(dataSource);
		//removes itens that already exists in the blueprint from being added again
		List<AuditItem> bpItens = editBP.getItens();
		
		List<AuditItem> toRemove = new ArrayList<AuditItem>();
		for (AuditItem bpAuditItem : bpItens) {
			for (AuditItem auditItem : allAuditItens) {
				if(auditItem.getId()==bpAuditItem.getId()) {
					toRemove.add(auditItem);
				}
			}
		}
		for (AuditItem auditItem : toRemove) {
			allAuditItens.remove(auditItem);
		}

		
		
		TreeSet<String> bluePrintCategories = new TreeSet<String>();
		TreeSet<String> categories = new TreeSet<String>();
		//initialize blueprint
		
		//running through itens, populating categories
		for (AuditItem item : allAuditItens) {
			categories.add(item.getCategory());
		}
		for (AuditItem item : bpItens) {
			bluePrintCategories.add(item.getCategory());
		}
		
		request.setAttribute("blueprintsCategories", bluePrintCategories);
		request.setAttribute("allAuditItens", allAuditItens);
		request.setAttribute("categories", categories);
		request.setAttribute("blueprint", editBP);
		request.setAttribute("blueprintItens", bpItens);
		
				
		request.getSession().setAttribute("allAuditItens", allAuditItens);
		request.getSession().setAttribute("newBlueprint", editBP);
		
		dispatcher = request.getRequestDispatcher("/editAuditBlueprint.jsp");
		dispatcher.forward(request, response);
		
	}


	private void listAudits(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		List<AuditBlueprint> blueprints =  AuditBlueprintDao.getAllAudit(dataSource);
		
		AuditItensDao.populateBlueprints(blueprints,dataSource);
		
		request.setAttribute("blueprints", blueprints);
		
		dispatcher = request.getRequestDispatcher("/auditBlueprint.jsp");
		dispatcher.forward(request, response);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
