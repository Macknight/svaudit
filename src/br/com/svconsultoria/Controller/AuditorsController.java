package br.com.svconsultoria.Controller;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import br.com.svconsultoria.DAO.UserDao;
import br.com.svconsultoria.Model.User;

/*
 * this commando is responsible for all user related activity
 * it handles all auditors functonality (add, delete, and update user)
 * and handle back an updated list to the in auditors.jsp
 * 
 * 
 * 
 */





@WebServlet("/AuditorsController")
public class AuditorsController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	@Resource(name = "jdbc/SVAuditDB")
	private DataSource dataSource;

	RequestDispatcher dispatcher;

	public AuditorsController() {
		super();

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String command = request.getParameter("command");
		
		switch (command) {
		case "show":
			listUsers(request, response);	
			break;
		case "add":
		
			addUser(request, response);	
			listUsers(request, response);
			break;
		case "delete":
			
			deleteUser(request, response);	
			listUsers(request, response);
			break;
		case "edit":
			
			showEditUserPage(request, response);
			break;

	
		case "update":
			
			saveUser(request,response);
			listUsers(request,response);
			break;
			
		default:
			listUsers(request, response);
			break;
		}
		
	}

	private void saveUser(HttpServletRequest request, HttpServletResponse response) {
		// TODO Auto-generated method stub
		
	}

	private void showEditUserPage(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int id = Integer.valueOf(request.getParameter("editId"));
		User editUser = UserDao.getUser(id, dataSource);
		request.setAttribute("user", editUser);
		dispatcher = request.getRequestDispatcher("/editUser.jsp");
		dispatcher.forward(request, response);
		
	}

	private void deleteUser(HttpServletRequest request, HttpServletResponse response) {
		int id = Integer.valueOf(request.getParameter("deleteId"));
		UserDao.deactivateUser(id, dataSource);
	}

	private void addUser(HttpServletRequest request, HttpServletResponse response) {
		
	
		String name = request.getParameter("name");
		String login= request.getParameter("login");
		String category = request.getParameter("category");
		String password= request.getParameter("password");
		if(name==null || name.equals("")) {
			System.out.println("name null");
		}
		if(login==null || login.equals("")) {
			System.out.println("login null");
		}
		if(category==null || category.equals("")) {
			System.out.println("category null");
		}
		if(password==null || password.equals("")) {
			System.out.println("password null");
		}
		User newUser = new User(name, password, category, login, true);
		UserDao.saveNewUser(newUser, dataSource);
	}

	private void listUsers(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		List<User> auditors = UserDao.getActiveUsersByType(User.USER_AUDITOR, dataSource);
		request.setAttribute("auditorsList", auditors);
		dispatcher = request.getRequestDispatcher("/auditors.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
