package br.com.svconsultoria.Utils;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;


public class RequestUtils {
	
	/**
	 * 
	 * @param request
	 * @return the url used to access the page
	 */
	public static String getCurrentUrl(HttpServletRequest request) {
		StringBuffer requestURL = request.getRequestURL();
		if (request.getQueryString() != null) {
		    requestURL.append("?").append(request.getQueryString());
		}
		String completeURL = requestURL.toString();
		return completeURL;
	}
/**
 *  @param request
 * 	@return void
 * list in the console all parameters and attributes on the request, its a facility tool
 * to see if all the parameters are being set in the interface!
 */
	public static void printRequestParameters(HttpServletRequest request, String controller) {


		System.out.println("from: "  + controller);
		Enumeration<String> parameterNames = request.getParameterNames();
		Enumeration<String> attributeNames = request.getAttributeNames();
		System.out.print("Parameters:  ");
		while (parameterNames.hasMoreElements()) {

			String paramName = parameterNames.nextElement();
			System.out.print( "[" + paramName + "]" );

		}
		System.out.println();
		System.out.print("Attributes: ");
		while (attributeNames.hasMoreElements()) {

			String AttributeName = attributeNames.nextElement();
			System.out.println(AttributeName );
			System.out.print( "[" + AttributeName  + "]" );

		}
		System.out.println( );

	}
	
	
	
	
	
	

}
