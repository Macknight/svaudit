package br.com.svconsultoria.DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;



import br.com.svconsultoria.Model.User;

public class UserDao extends DAO{

	public static String INSERT_NEW_USER = "INSERT INTO T_USER (C_NAME, C_USER_CATEGORY, C_PASSWORD, C_LOGIN, C_ATIVO) VALUES (?,?,?,?,?);";
	public static String DELETE_USER = "DELETE FROM T_USER WHERE PK_USER_ID = ?;";
	public static String DEACTIVATE_USER= "UPDATE T_USER SET C_ATIVO = 0 WHERE PK_USER_ID = ?";
			
	
	
	public static User getUser(int id, DataSource dataSource) {

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		try {
			conn = dataSource.getConnection();
			User retUser = null;
			String sql = "SELECT * FROM T_USER WHERE PK_USER_ID=" + id;
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);

		   retUser = getUserFromResultSet(rs);

			return retUser;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}

		return null;

	}


	public static User getUserByLogin(String login, DataSource dataSource) {
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		try {
			conn = dataSource.getConnection();
			User retUser = null;
			String sql = "SELECT * FROM T_USER WHERE C_LOGIN='" + login + "'";
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);

			retUser = getUserFromResultSet(rs);

			return retUser;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;

	}

	public static User getUserFromResultSet(ResultSet rs) {
		User retUser = null;

		try {
			if (rs.next()) {
				int id = rs.getInt("PK_USER_ID");
				String login = rs.getString("C_LOGIN");
				String name = rs.getString("C_NAME");
				String category = rs.getString("C_USER_CATEGORY");
				String password = rs.getString("C_PASSWORD");

				retUser = new User(id, name, password, category, login);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retUser;
	}
	
	public static List<User> getUserSFromResultSet(ResultSet rs) {
		List<User> retUsers = null;
		retUsers = new ArrayList<User>();
		try {
			while (rs.next()) {
				int id = rs.getInt("PK_USER_ID");
				String login = rs.getString("C_LOGIN");
				String name = rs.getString("C_NAME");
				String category = rs.getString("C_USER_CATEGORY");
				String password = rs.getString("C_PASSWORD");

				retUsers.add(new User(id, name, password, category, login)); 

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return retUsers;
	}
	
	/*
	 * Method that return each group of users (admin, auditors)
	 * */
	public static List<User> getUsersByType(int UserType, DataSource dataSource) {
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<User> retList;
		try {
			conn = dataSource.getConnection();
			
			String sql = "SELECT * FROM T_USER WHERE C_USER_CATEGORY=" + UserType;
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);

			retList = getUserSFromResultSet(rs);

			return retList;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}

		return null;
	}
	
	
	public static List<User> getActiveUsersByType(int UserType, DataSource dataSource) {
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<User> retList;
		try {
			conn = dataSource.getConnection();
			
			String sql = "SELECT * FROM T_USER WHERE C_USER_CATEGORY=" + UserType;
			sql+=" AND C_ATIVO=1";
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);

			retList = getUserSFromResultSet(rs);

			return retList;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}

		return null;
	}
	
	


	public static int saveNewUser(User newUser, DataSource dataSource) {

		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(INSERT_NEW_USER) ;
			
			ps.setString(1, newUser.getName());
			ps.setString(2,newUser.getCategory());
			ps.setString(3,newUser.getPassword());
			ps.setString(4, newUser.getLogin());
			ps.setBoolean(5, newUser.getAtivo());
		
			int result = ps.executeUpdate();

		
			return result;
			
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}
		return 0;

		
	}


	public static void deleteUser(int id, DataSource dataSource) {
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(DELETE_USER) ;
			
			ps.setInt(1, id);
		
		
			int result = ps.executeUpdate();

			System.out.println(result +  " rows affected" );
		
			
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}
		
		
	}
	
	
	public static void deactivateUser(int id, DataSource dataSource) {
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		try {
			conn = dataSource.getConnection();
			PreparedStatement ps = conn.prepareStatement(DEACTIVATE_USER) ;
			
			ps.setInt(1, id);
		
		
			int result = ps.executeUpdate();

			System.out.println(result +  " rows affected" );
		
			
			

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}
		
		
	}

}
