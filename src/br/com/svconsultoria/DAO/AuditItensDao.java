package br.com.svconsultoria.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import  java.sql.PreparedStatement;

import br.com.svconsultoria.Model.AuditBlueprint;
import br.com.svconsultoria.Model.AuditItem;

public class AuditItensDao extends DAO {

	private static final String INSERT_NEW_ITEM = "INSERT INTO T_AUDIT_ITEM (C_AUDIT_ITEM_CATEGORY, C_QUESTION, C_ATIVO) VALUES(?,?,?)";
	private static final String DEACTIVATE_ITEM = "UPDATE T_AUDIT_ITEM SET C_ATIVO = 0 WHERE PK_AUDIT_ITEM_ID = ?";
	private static final String UPDATE_ITEM = "UPDATE T_AUDIT_ITEM SET C_ATIVO = 1, C_QUESTION=?, C_AUDIT_ITEM_CATEGORY = ? WHERE PK_AUDIT_ITEM_ID = ?";

	public static List<AuditItem> getAllAuditItens(DataSource dataSource) {
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<AuditItem> retList;
		try {
			conn = dataSource.getConnection();

			String sql = "SELECT * FROM T_AUDIT_ITEM " ;
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);

			retList = getItensFromResultSet(rs);

			return retList;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}

		return null;

	}
	
	public static List<AuditItem> getAllActiveAuditItens(DataSource dataSource) {
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<AuditItem> retList;
		try {
			conn = dataSource.getConnection();

			String sql = "SELECT * FROM T_AUDIT_ITEM WHERE C_ATIVO = 1" ;
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);

			retList = getItensFromResultSet(rs);

			return retList;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}

		return null;

	}

	
	

	private static List<AuditItem> getItensFromResultSet(ResultSet rs) {
		List<AuditItem> itensList = new ArrayList<AuditItem>();
		try {
			while (rs.next()) {
				int id = rs.getInt("PK_AUDIT_ITEM_ID");

				String question = rs.getString("C_QUESTION");

				String category = rs.getString("C_AUDIT_ITEM_CATEGORY");

				AuditItem item = new AuditItem(id, question, category);
				itensList.add(item);
			}
			return itensList;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static void populateBlueprints(List<AuditBlueprint> blueprints, DataSource dataSource) {

		for (AuditBlueprint auditBlueprint : blueprints) {
			populateBlueprint(auditBlueprint, dataSource);

		}

	}

	public  static void populateBlueprint(AuditBlueprint auditBlueprint, DataSource dataSource) {
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;

		try {	
			conn = dataSource.getConnection();

			String sql = "SELECT * FROM t_audit_item ITENS, t_blueprint_itens BLUEPRINT WHERE "
					+ "ITENS.PK_AUDIT_ITEM_ID = BLUEPRINT.FK_AUDIT_ITEM_ID AND BLUEPRINT.FK_BLUEPRINT_ID = ";
			sql += auditBlueprint.getId() + ";";

			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			getItemFromResultSet(rs,auditBlueprint);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);	
		}

	}

	private static void getItemFromResultSet(ResultSet rs, AuditBlueprint auditBlueprint) throws SQLException {
	 while(rs.next()) {
		 int id= rs.getInt("PK_AUDIT_ITEM_ID");
		 String question = rs.getString("C_QUESTION");
		 String category = rs.getString("C_AUDIT_ITEM_CATEGORY");
		 boolean mandatory = rs.getBoolean("B_MANDATORYT");
		 auditBlueprint.getItens().add(new AuditItem(id,question
				,category, mandatory ));
		 
	 }
	}

	public static void insertNewAuditItem(AuditItem newAuditItem, DataSource dataSource) {
		Connection conn = null;
		PreparedStatement statement = null;
		ResultSet rs = null;

		try {
			conn = dataSource.getConnection();

			
			

			statement = conn.prepareStatement(INSERT_NEW_ITEM);
			//category preceds question in the table layout
			statement.setString(1,newAuditItem.getCategory());
			statement.setString(2,newAuditItem.getQuestion());
			//set item to be active by default 
			statement.setBoolean(3, true);
		    statement.executeUpdate();
			
			
		
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}
	}

	public static void deleteItem(int itemId, DataSource dataSource) {
		Connection conn = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
	
		try {
			conn = dataSource.getConnection();

			statement = conn.prepareStatement(DEACTIVATE_ITEM);
			statement.setInt(1, itemId);
			statement.executeUpdate();

		

		

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}

	

		
	}

	public static void updateItem(AuditItem updatedAuditItem, DataSource dataSource) {
		Connection conn = null;
		PreparedStatement statement = null;
		ResultSet rs = null;
	
		try {
			conn = dataSource.getConnection();

			statement = conn.prepareStatement(UPDATE_ITEM);
			statement.setString(1, updatedAuditItem.getQuestion());
			statement.setString(2, updatedAuditItem.getCategory());
			statement.setInt(3, updatedAuditItem.getId());
			statement.executeUpdate();

		

		

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}

		
	}

}
