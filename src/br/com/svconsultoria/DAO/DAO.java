package br.com.svconsultoria.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

public class DAO {

	public static void close(Connection conn, Statement statement, ResultSet rs) {
		try {
			if (conn != null) {
				conn.close();
			}
			if (statement != null) {
				statement.close();
			}
			if (rs != null) {
				rs.close();
			}
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

}
