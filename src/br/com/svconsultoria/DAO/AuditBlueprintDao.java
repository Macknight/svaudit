package br.com.svconsultoria.DAO;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import java.sql.PreparedStatement;

import br.com.svconsultoria.Model.AuditBlueprint;
import br.com.svconsultoria.Model.AuditItem;

public class AuditBlueprintDao extends DAO{
	
	public static final String GET_BLUEPRINT_BY_ID = "SELECT * FROM T_AUDIT_BLUEPRINT WHERE PK_BLUEPRINT_ID = ?";
	public static final String INSERT_NEW_BLUEPRINT= "INSERT INTO t_audit_blueprint (C_NAME) VALUES (?)";
	public static final String INSERT_NEW_BLUEPRINT_ITEMS= "INSERT INTO t_blueprint_itens (FK_BLUEPRINT_ID, FK_AUDIT_ITEM_ID, B_MANDATORYT) VALUES (?, ?, ?)";
	public static final String DELETE_BLUEPRINT_ITENS = "DELETE FROM T_BLUEPRINT_ITENS WHERE FK_BLUEPRINT_ID = ?";

	public static List<AuditBlueprint> getAllAudit(DataSource dataSource){
		Connection conn = null;
		Statement statement = null;
		ResultSet rs = null;
		List<AuditBlueprint> retList;
		try {
			conn = dataSource.getConnection();
			
			String sql = "SELECT * FROM t_audit_blueprint";
			statement = conn.createStatement();
			rs = statement.executeQuery(sql);
			
			retList = getAuditsFromResultSet(rs);

			return retList;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}

		return null;
		
	}

	private static List<AuditBlueprint> getAuditsFromResultSet(ResultSet rs) throws SQLException {
		List<AuditBlueprint> ret =  new ArrayList<AuditBlueprint>();
		while(rs.next()) {
			AuditBlueprint next = new AuditBlueprint(rs.getInt("PK_BLUEPRINT_ID"),rs.getString("C_NAME"));
			ret.add(next);
		}
		return ret;
	}
	
	private static AuditBlueprint getAuditFromResultSet(ResultSet rs) throws SQLException {
		AuditBlueprint ret = null;
		if(rs.next()) {
			ret = new AuditBlueprint(rs.getInt("PK_BLUEPRINT_ID"),rs.getString("C_NAME"));
			
		}
		return ret;
	}
	

	public static AuditBlueprint getBlueprint(int editId, DataSource dataSource) {
		Connection conn = null;
		PreparedStatement statement  = null;
		ResultSet rs = null;
		AuditBlueprint blueprint = null;
		try {
			conn = dataSource.getConnection();
			
			
			statement = conn.prepareStatement(GET_BLUEPRINT_BY_ID);
			statement.setInt(1, editId);
			rs = statement.executeQuery();
			
			blueprint = getAuditFromResultSet(rs);

			return blueprint;

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}

		return null;
		
	}

	

	public static void saveBlueprint(AuditBlueprint newBlueprint, DataSource dataSource) {
		Connection conn = null;
		PreparedStatement statement  = null;
		ResultSet rs = null;
		
		try {
			conn = dataSource.getConnection();


			statement = conn.prepareStatement(INSERT_NEW_BLUEPRINT,Statement.RETURN_GENERATED_KEYS);
			statement.setString(1, newBlueprint.getName());
			statement.executeUpdate();

			rs = statement.getGeneratedKeys();
			rs.next();
			int blueprintId= rs.getInt(1);
			newBlueprint.setId(blueprintId);
			
			
			for (AuditItem item : newBlueprint.getItens()) {
				statement = conn.prepareStatement(INSERT_NEW_BLUEPRINT_ITEMS);
				statement.setInt(1, newBlueprint.getId());
				statement.setInt(2, item.getId());
				statement.setBoolean(3, false);
				statement.executeUpdate();
			}
			




		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}

	}
	
	
	
   public static void updateBlueprintItens(AuditBlueprint bp, DataSource dataSource) {
	    Connection conn = null;
		PreparedStatement statement  = null;
		ResultSet rs = null;
		
		try {
			conn = dataSource.getConnection();
			

			deleteBlueprintItens( bp,  dataSource);
			
		
			for (AuditItem item : bp.getItens()) {
				statement = conn.prepareStatement(INSERT_NEW_BLUEPRINT_ITEMS);
				statement.setInt(1, bp.getId());
				statement.setInt(2, item.getId());
				statement.setBoolean(3, false);
				statement.executeUpdate();
			}
			




		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close(conn, statement, rs);
		}
   }

private static void deleteBlueprintItens(AuditBlueprint bp, DataSource dataSource) {
	Connection conn = null;
	PreparedStatement statement  = null;
	ResultSet rs = null;
	
	try {
		conn = dataSource.getConnection();
		

		statement = conn.prepareStatement(DELETE_BLUEPRINT_ITENS);
		statement.setInt(1, bp.getId());
	
		statement.executeUpdate();
		



	} catch (Exception e) {
		e.printStackTrace();
	} finally {
		close(conn, statement, rs);
	}
	
}
}
