<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="header.html" />

<div class="row">
	<div class="container ">
		<ul class="nav nav-pills nav-fill">
			<li class="nav-item"><a class="nav-link "
				href="auditors?command=show">Auditores</a></li>
			<li class="nav-item"><a class="nav-link active"
				href="audits?command=show">Auditorias</a></li>
			<li class="nav-item"><a class="nav-link"
				href="itens?command=show">Itens de auditoria</a></li>
			<li class="nav-item"><a class="nav-link " href="reports.jsp">Relat�rios</a>
			</li>
		</ul>
	</div>
</div>

<hr/>

<h2 class="menuTitle" align="center">Novo modelo de Auditoria  </h2>


<div class="container-fluid ">
  <div class="row newAuditBlueprint " >
    <div class="col-sm newBlueprintLeft">
			<form id="newBlueprintForm" action="audits?command=saveBlueprint" method="POST">
				
					<label for="blueprintName">Nome da auditoria:</label>
					 <input
						type="text" class="form-control" id="blueprintName"
						placeholder="${newBlueprint.name}" name="blueprintName" >
						<br/>
						<br/>
						<!-- blueprint accordion -->
					<div class="col-sm">
						<div id="blueprintAccordion" role="tablist" aria-multiselectable="false">
							<%
								int bpCategoriesCounter = 1;
								int bpQuestionCounter = 1;
							%>
							<c:forEach var="blueprintCategory" items="${blueprintsCategories}">
								<div class="card">
									<div class="card-header" role="tab"
										id="bpHeading-<%=bpCategoriesCounter%>">
										<h5 class="mb-0">
											<a data-toggle="collapse" data-parent="#blueprintAccordion"
												href="#bpCollapse-<%=bpCategoriesCounter%>"
												aria-expanded="false"
												aria-controls="bpCollapse-<%=bpCategoriesCounter%>">
												${blueprintCategory} </a>
										</h5>
									</div>
	
									<div id="bpCollapse-<%=bpCategoriesCounter%>" class="collapse"
										role="tabpanel"
										aria-labelledby="bpHeading-<%=bpCategoriesCounter++%>">
										<div class="card-block">
											<table class="table table-bordered">
												<thead class="thead-inverse">
													<tr>
														<th>Item</th>
														<th class="removeField">Op��es</th>
														<th class="removeField">Remover</th>

													</tr>
												</thead>
												<c:forEach var="bpAuditItem" items="${blueprintItens}">
													<c:if test="${bpAuditItem.category == blueprintCategory}">
														<tbody>
															<tr>
																<td class="align-middle">${bpAuditItem.question}</td>
																<td>
																	<div class="form-check form-check-inline">
																		<label class="form-check-label"> 
																		<input
																			class="form-check-input" type="checkbox"
																			id="mandatoryCheckbox-<%=bpQuestionCounter%>"
																			 value="option-<%=bpQuestionCounter %>"> Obrigat�rio
																		</label>
													<input type="hidden" name="bpItem-<%=bpQuestionCounter++ %>"
													value="${auditItem.id}" />
																	</div>

																</td>
																<td>

																	<button type="submit" class="btn btn-secondary btn-lg"
																		data-toggle="tooltip" data-placement="top"
																		title="Remover item" name="deleteFromBlueprint">
																		<img
																			src="ResoucesShared/ic_fast_forward_white_24px.svg"
																			alt="Inserir">

																	</button> 
																	<input type="hidden" name="removeItemId"
																	value="${bpAuditItem.id}" />
													
																</td>
																	



															</tr>
															

														</tbody>
													</c:if>
												</c:forEach>

											</table>

										</div>
									</div>
								</div>

							</c:forEach>
						</div>
					</div>


				
			</form>
	</div>
	
	
	<!-- list with all the itens that can be inserted in this  -->
    <div class="col-sm">
    	<div id="accordion" role="tablist" aria-multiselectable="false">
    	<%
			int categoriesCounter = 1;
			int questionCounter = 1;
		%>
				<c:forEach var="category" items="${categories}">
					<div class="card">
						<div class="card-header" role="tab"
							id="heading-<%=categoriesCounter%>">
							<h5 class="mb-0">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse-<%=categoriesCounter%>" aria-expanded="false"
									aria-controls="collapse-<%=categoriesCounter%>">
									${category} </a>
							</h5>
						</div>

						<div id="collapse-<%=categoriesCounter%>" class="collapse"
							role="tabpanel"
							aria-labelledby="heading-<%=categoriesCounter++%>">
							<div class="card-block">
								<table class="table table-bordered">
									<thead class="thead-inverse">
										<tr>
											<th>Inserir</th>
											<th>Item</th>

										</tr>
									</thead>	
									<c:forEach var="auditItem" items="${allAuditItens}">
										<c:if test="${auditItem.category == category}">
											<tbody>
												<tr>
													<!--  <td scope="row" class="align-middle"><%=questionCounter++%></th>-->
													<td>
													<form action="audits?command=insertItem" method="POST" 
													id="insertBtn">
														<button type="submit" onclick="getName()" class="btn btn-secondary btn-lg"
															data-toggle="tooltip" data-placement="top" title="Inserir item">
															<img src="ResoucesShared/ic_fast_rewind_white_24px.svg"
																alt="Inserir">

														</button>
														<input type="hidden" name="insertItemId"
															value="${auditItem.id}" />
													</form>
													</td>
													<td class="align-middle">${auditItem.question}</td>



												</tr>

											</tbody>
										</c:if>
									</c:forEach>
								
								</table>

							</div>
						</div>
					</div>

				</c:forEach>
			</div>
    </div>
    
  </div>
</div>


<div class="container botButtonsDiv">
	
	<button type="submit" form="newBlueprintForm" class="btn btn-primary btn-lg botButtons">Salvar Auditoria</button>
	<a href="audits?command=show"><button type="button" class="btn btn-secondary btn-lg botButtons">Cancelar</button></a>

</div>



<script>
	function getName() {
		var auditName;
		var btnForm;
		auditName = document.getElementById('blueprintName').value;
		btnForm = document.getElementById('insertBtn');
		btnForm.action = "audits?command=insertItem&blueprintName=" + auditName;
		
		

		return true;
	}
</script>



<jsp:include page="footer.jsp" />