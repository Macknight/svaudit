<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.html" />






<div class="container">

	<form action="auditors?command=update" method="POST" >
		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="nome">Nome</label> <input type="text"
					class="form-control" id="nome" value="${user.name }" placeholder="${user.name}">
			</div>
		</div>
		<div class="form-group">
			<label for="inputAddress">Login</label> <input type="text"
				class="form-control" id="inputAddress" value="${user.login}">
		</div>

		<div class="form-row">
			<div class="form-group col-md-6">
				<label for="password">Password</label> <input type="password"
					class="form-control" id="password" placeholder="Password">
			</div>
			<div class="form-group col-md-6">
				<label for="confirmPassword">Confirmar Password</label> <input type="password"
					class="form-control" id="confirmPassword" placeholder="Password">
			</div>

		</div>


		
		<div class="form-group">
			<label for="category">Categoria:</label> <input type="text"
				class="form-control" id="category"
				value="${user.category}">
		</div>
		<div class="form-row">
			
			<div class="form-group col-md-4">
				<label for="inputState">Estado:</label> <select id="inputState"
					class="form-control">
					<option selected>Ativo</option>
					<option>Inativo</option>
				</select>
			</div>
		
		</div>
		
		<button type="submit" class="btn btn-primary">Salvar usu�rio</button>
	</form>

</div>












<jsp:include page="footer.jsp" />
