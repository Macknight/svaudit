<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.html" />


<div class="row">
	<div class="container ">
		<ul class="nav nav-pills nav-fill">
			<li class="nav-item"><a class="nav-link active"
				href="auditors?command=show">Auditores</a></li>
			<li class="nav-item"><a class="nav-link " href="audits?command=show">Auditorias</a></li>
			<li class="nav-item"><a class="nav-link" href="itens?command=show">Itens
					de auditoria</a></li>
			<li class="nav-item"><a class="nav-link " href="reports.jsp">Relat�rios</a>
			</li>
		</ul>
	</div>
</div>


<hr/>

<%int auditorCounter = 1; %>

<section id="showAuditors">
	<div class="container ">
	<h2>Auditores:</h2>
		<table class="table  table-hover table-bordered">
			<thead class="thead-dark">
				<tr>
					<th>#</th>
				<!-- 	<th>Id</th>  unccoment for showing id-->
					<th>Nome</th>
					<th>Categoria</th>
					<th>Login</th>
					<th>Editar</th>
					<th>Excluir</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="auditor" items="${auditorsList}">
					<tr>
						<th scope="row"><%=auditorCounter++ %></th>
				<!-- 		<th scope="row">${auditor.id }</th>for showing id-->
						<td>${auditor.name}</td>
						<td>${auditor.category}</td>
						<td>${auditor.login}</td>
						<td>
							<form action="auditors?command=edit" method="POST">
								<button type="submit" class="btn btn-secondary btn-lg">
									<img src="ResoucesShared/ic_mode_edit_black_24px.svg"
										alt="editar" data-toggle="tooltip" data-placement="top"
										title="Editar">
								</button>
								<input type="hidden" name="editId" value="${auditor.id}" />
								
							</form>
						</td>
						<td>
							<form action="auditors?command=delete" method="POST">
								<button type="submit" class="btn btn-secondary btn-lg">
									<img src="ResoucesShared/ic_delete_sweep_black_24px.svg"
										alt="excluir" data-toggle="tooltip" data-placement="top"
										title="Excluir">
								</button>
								<input type="hidden" name="deleteId" value="${auditor.id}" />
							</form>

						</td>

					</tr>


				</c:forEach>
				<tr>
					<td colspan="6" data-toggle="tooltip" data-placement="top"
							title="Adicionar auditor" >
						<button type="button" class="btn btn-secondary  btn-lg btn-block"
						data-toggle="modal" data-target="#addUserModal">
							<img src="ResoucesShared/ic_person_add_white_24px.svg"
								alt="excluir">
						</button>
					</td>
				</tr>
			</tbody>
		</table>

	</div>


	<!-- container for the 'add user' popup -->

	<div class="container">
		<div class="modal fade" id="addUserModal" tabindex="-1" role="dialog"
			aria-labelledby="modalTitle" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="modalTitle">Adicionar usu�rio</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">

						<form action="auditors?command=add" method="POST">
							<div class="form-row">
								<div class="form-group col-md-6">
									<label for="name">Nome</label> 
									<input type="text" name="name"
										class="form-control" id="name" placeholder="Nome">
								</div>

							</div>
							<div class="form-group">
								<label for="category">Categoria</label>
								 <input type="text" name="category"
									class="form-control" id="category"
									placeholder="categoria (1,2,3...)">
							</div>
							<div class="form-group">
								<label for="login">Login</label> <input type="text" name="login"
									class="form-control" id="login" placeholder="Nome de usu�rio.">
							</div>
							<div class="form-group ">
								<label for="password">Password</label> <input type="password" name="password"
									class="form-control" id="password" placeholder="Password">
							</div>

							<div class="modal-footer">
								<button type="button" class="btn btn-secondary"
									data-dismiss="modal">Cancelar</button>
								<button  type = "submit" value = "Submit" class="btn btn-primary">Salvar
									Usu�rio</button>
							</div>
						</form>


					</div>

				</div>
			</div>
		</div>

	</div>
</section>
<jsp:include page="footer.jsp" />
