<jsp:include page="header.html" />


<div class="row">
	<div class="container">
		<ul class="nav nav-pills nav-fill">
			<li class="nav-item"><a class="nav-link " href="auditors?command=show">Auditores</a></li>
			<li class="nav-item"><a class="nav-link" href="audits?command=show">Auditorias</a></li>
			<li class="nav-item"><a class="nav-link" href="itens?command=show">Itens de auditoria</a></li>
			<li class="nav-item"><a class="nav-link " href="reports.jsp">Relatórios</a>
			</li>
		</ul>
	</div>
</div>


<jsp:include page="footer.jsp" />
