<%@ page import="br.com.svconsultoria.Controller.LoginController" %>

<jsp:include page="header.html"/>

<hr>
<%
   String loginTry =  (String) request.getAttribute(LoginController.LOGIN_RESPONSE);
%>

<div class="container"  >	
		<div class="row  justify-content-center align-items-center">
			<form action="LoginCommand" class="form-inline" method="POST">
				<div class="form-group">
					
						<label for="login">Usu�rio: </label>
						 <input type="text" required placeholder="Usu�rio" id="login" name="login">
						<pre>   </pre>						
					
							<label for="password">Senha:</label> 
							<input type="password" required placeholder="Senha"
							name="password" id="password"> 
					
					<input class="btn btn-danger"
							type="submit" name="Entrar" value="Entrar">

				</div>
			</form>
		</div>
	
</div>


<!--  the following script show the user a message in case of failed login  -->
<%if(loginTry!=null){%>
	<%if(loginTry.equals(LoginController.USER_NOT_FOUND)){ %>
	<div class="container">
<div class="alert alert-warning" role="alert">
	<strong>Erro!</strong> Usu�rio n�o existe, verifique seu Login
</div>
</div>
<%} %>
<%if(loginTry.equals(LoginController.PASSWORD_INCORRECT)){ %>
<div class="container">
	<div class="alert alert-warning" role="alert">
		<strong>Erro!</strong>Sua senha est� incorreta
	</div>
</div>
<%} %>
<%}//login!=null %>




<hr>
<jsp:include page="footer.jsp"/>