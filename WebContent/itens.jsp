<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="header.html" />


<div class="row">
	<div class="container ">
		<ul class="nav nav-pills nav-fill">
			<li class="nav-item"><a class="nav-link "
				href="auditors?command=show">Auditores</a></li>
			<li class="nav-item"><a class="nav-link "
				href="audits?command=show">Auditorias</a></li>
			<li class="nav-item"><a class="nav-link active"
				href="itens?command=show">Itens de auditoria</a></li>
			<li class="nav-item"><a class="nav-link " href="reports.jsp">Relat�rios</a>
			</li>
		</ul>
	</div>
</div>
<hr/>
<%
	int categoriesCounter = 1;
	int questionCounter = 1;
%>


<section id="showItens">
	<div class=container>
		<h2>Itens de Auditoria:</h2>
		<div id="accordion" role="tablist" aria-multiselectable="false">
			<c:forEach var="category" items="${categories}">
				<div class="card">
					<div class="card-header" role="tab"
						id="heading-<%=categoriesCounter%>">
						<h5 class="mb-0">
							<a data-toggle="collapse" data-parent="#accordion"
								href="#collapse-<%=categoriesCounter%>" aria-expanded="false"
								aria-controls="collapse-<%=categoriesCounter%>"> ${category}
							</a>
						</h5>
					</div>

					<div id="collapse-<%=categoriesCounter%>" class="collapse"
						role="tabpanel" aria-labelledby="heading-<%=categoriesCounter++%>">
						<div class="card-block">
							<table class="table table-bordered">
								<thead class="thead-inverse">
									<tr>
										<th>#</th>
										<th>Item</th>
										<th>Editar</th>
										<th>Excluir</th>
									</tr>
								</thead>
								<c:forEach var="auditItem" items="${itens}">
									<c:if test="${auditItem.category == category}">
										<tbody>
											<tr>
												<th scope="row" class="align-middle"><%=questionCounter++%></th>
												<td class="align-middle">${auditItem.question}</td>
												<td>
													<div data-toggle="tooltip" data-placement="top" title="Editar">
														<button type="button" class="btn btn-secondary btn-lg"
															data-toggle="modal" data-target="#editItemModal${auditItem.id}">
														
															<img src="ResoucesShared/ic_mode_edit_black_24px.svg"
																alt="editar">
														</button>
													</div>
												</td>
												<td>
													<form action="itens?command=delete" method="POST">
														<button type="submit" class="btn btn-secondary btn-lg"
															data-toggle="tooltip" data-placement="top" title="Editar">
															<img src="ResoucesShared/ic_delete_sweep_black_24px.svg"
																alt="excluir">

														</button>
														<input type="hidden" name="deleteId"
															value="${auditItem.id}" />
													</form>
												</td>

											</tr>

										</tbody>
									</c:if>
								</c:forEach>
								<%
									questionCounter = 1;
								%>
							</table>

						</div>
					</div>
				</div>

			</c:forEach>
		</div>
		<div class="" data-toggle="tooltip" data-placement="top"
			title="Inserir item de auditoria">
			<button type="button" class="btn btn-secondary btn-lg btn-block"
				data-toggle="modal" data-target="#addItemModal">
				<img src="sResoucesShared/ic_playlist_add_white_24px.svg"
					alt="inserir">
			</button>

		</div>
	</div>




	<!-- ITEN EDIT MODALS -->
	<c:forEach var="auditItem" items="${itens}">
	<div class="container">
		<div class="modal fade" id="editItemModal${auditItem.id}" tabindex="-1" role="dialog"
			aria-labelledby="modalTitle" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="modalTitle">Alterar item de
							auditoria.</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">

						<form action="itens?command=edit" method="POST">
							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="itemDescription">Descri��o do item.</label> 
									<input value="${auditItem.question}"
										type="text" name="itemDescription" class="form-control"
										id="itemDescription" placeholder="Descri��o do item">
								</div>

							</div>
							<label class="mr-sm-2" for="categoryID">Categoria: </label><br />
							<select class="custom-select mb-2 mr-sm-2 mb-sm-0"
								id="categoryID" name="categoryId">

								<c:forEach var="category" items="${categories}">
									<option value="${category}"
									<c:if test= "${auditItem.category == category}" >
										selected
									 </c:if>
									
									>${category}</option>
								</c:forEach>

							</select>

							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="newCategory">Ou adicione uma nova
										categoria.(Deixe em branco caso tenha para usar uma das
										existentes)</label> <input type="text" name="newCategory"
										class="form-control" id="newCategory"
										placeholder="Deixe em branco caso queira utilizar uma j� existente.">
								</div>

							</div>
	

							<div class="modal-footer">
								<button type="button" class="btn btn-secondary"
									data-dismiss="modal">Cancelar</button>
								<button type="submit" value="Submit" class="btn btn-primary">Salvar
									Altera��es</button>
							</div>
							<input type="hidden" name="itemID"
															value="${auditItem.id}" />
						</form>


					</div>

				</div>
			</div>
		</div>

	</div>
	</c:forEach>



	<!-- ITEN INSERTION MODAL -->
	<div class="container">
		<div class="modal fade" id="addItemModal" tabindex="-1" role="dialog"
			aria-labelledby="modalTitle" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="modalTitle">Adicionar item de
							auditoria.</h5>
						<button type="button" class="close" data-dismiss="modal"
							aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">

						<form action="itens?command=add" method="POST">
							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="itemDescription">Descri��o do item.</label> <input
										type="text" name="itemDescription" class="form-control"
										id="itemDescription" placeholder="Descri��o do item">
								</div>

							</div>
							<label class="mr-sm-2" for="categoryID">Categoria: </label><br />
							<select class="custom-select mb-2 mr-sm-2 mb-sm-0"
								id="categoryID" name="categoryId">

								<c:forEach var="category" items="${categories}">
									<option value="${category}">${category}</option>
								</c:forEach>

							</select>

							<div class="form-row">
								<div class="form-group col-md-12">
									<label for="newCategory">Ou adicione uma nova
										categoria.(Deixe em branco caso tenha para usar uma das
										existentes)</label> <input type="text" name="newCategory"
										class="form-control" id="newCategory"
										placeholder="Deixe em branco caso queira utilizar uma j� existente.">
								</div>

							</div>


							<div class="modal-footer">
								<button type="button" class="btn btn-secondary"
									data-dismiss="modal">Cancelar</button>
								<button type="submit" value="Submit" class="btn btn-primary">Salvar
									Item</button>
							</div>
						</form>


					</div>

				</div>
			</div>
		</div>

	</div>
</section>

<jsp:include page="footer.jsp" />
