<jsp:include page="header.html" />


<div class="row">
	<div class="container ">
		<ul class="nav nav-pills nav-fill">
			<li class="nav-item"><a class="nav-link " href="auditors?command=show">Auditores</a></li>
			<li class="nav-item"><a class="nav-link " href="audits?command=show">Auditorias</a></li>
			<li class="nav-item" ><a class="nav-link" href="itens?command=show">Itens de auditoria</a></li>
			<li class="nav-item"><a class="nav-link active" href="reports.jsp">Relat�rios</a>
			</li>
		</ul>
	</div>
</div>

<hr/>



    <div class="container-fluid">	
      <div class="row ">
      
      	<!-- parameters  
		<nav class="col-sm-3 col-md-2 hidden-xs-down bg-faded sidebar">
			<ul class="nav nav-pills flex-column ">
				<li class="nav-item"><a class="nav-link active" href="#">Param�tros
						de consulta: <span class="sr-only">(current)</span>
				</a></li>

			</ul>
			<div class="jumbotron ">
				<ul class="nav nav-pills flex-column">
					<li class="nav-item"><a class="nav-link" href="#">Data
							Inicial</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Data
							final</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Nome</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Id</a></li>
				</ul>
			</div>
		</nav>
		
		-->
		<!-- report types -->
        <main class="col-sm-9 col-md-10 pt-3">
      
	
          <section class="row text-center placeholders">
            <div class="col-6 col-sm-3 placeholder" >
              <img src="ResoucesShared/Users.png" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
              <h4>Usu�rios</h4>
              <div class="text-muted">Resumido | Detalhado</div>
            </div>
            <div class="col-6 col-sm-3 placeholder">
              <img src="ResoucesShared/models.png" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
              <h4>Modelos de Auditorias</h4>
              <span class="text-muted">Resumido | Detalhado</span>
            </div>
            <div class="col-6 col-sm-3 placeholder">
              <img src="ResoucesShared/doneAudits.png" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
              <h4>Auditorias</h4>
              <span class="text-muted">Resumido | Detalhado</span>
            </div>
            <div class="col-6 col-sm-3 placeholder">
              <img src="ResoucesShared/itens.png" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
              <h4>Itens de Auditoria.</h4>
              <span class="text-muted">Resumido | Detalhado</span>
            </div>
          </section>
	<!--  report data 
          <h2>report-type</h2>
          <div class="table-responsive">
            <table class="table table-striped offset-bottom">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Field1</th>
                  <th>Field2</th>
                  <th>Field3</th>
                  <th>Field4</th>
                  <th>Field5</th>
                  <th>Field6</th>
                  <th>Field7</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>1,001</td>
                  <td>Lorem</td>
                  <td>ipsum</td>
                  <td>dolor</td>
                  <td>sit</td>
                  <td>sit</td>
                  <td>sit</td>
                  <td>sit</td>
                </tr>
                <tr>
                  <td>1,002</td>
                  <td>amet</td>
                  <td>amet</td>
                  <td>amet</td>
                  <td>amet</td>
                  <td>consectetur</td>
                  <td>adipiscing</td>
                  <td>elit</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>Integer</td>
                  <td>Integer</td>
                  <td>Integer</td>
                  <td>Integer</td>
                  <td>nec</td>
                  <td>odio</td>
                  <td>Praesent</td>
                </tr>
                <tr>
                  <td>1,003</td>
                  <td>libero</td>
                  <td>Sed</td>
                  <td>cursus</td>
                  <td>ante</td>
                  <td>ante</td>
                  <td>ante</td>
                  <td>ante</td>
                </tr>
                <tr>
                  <td>1,004</td>
                  <td>dapibus</td>
                  <td>diam</td>
                  <td>Sed</td>
                  <td>nisi</td>
                  <td>nisi</td>
                  <td>nisi</td>
                  <td>nisi</td>
                </tr>
             
              </tbody>
            </table>
          </div>
          
          
          -->
        </main>
      </div>
    </div>

