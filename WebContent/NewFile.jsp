<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.html" />

<div class="container">
  <div class="row">
    <div class="col-sm">
      <form>
			<div class="col-6">
				<label for="blueprintName">Nome da auditoria:</label> <input
					type="text" class="form-control" id="blueprintName"
					placeholder="Leadership Audit">

				<hr>
				<hr>
			</div>
		</form>
    </div>
    <div class="col-sm">
    	<div id="accordion" role="tablist" aria-multiselectable="false">
    	<%
			int categoriesCounter = 1;
			int questionCounter = 1;
		%>
				<c:forEach var="category" items="${categories}">
					<div class="card">
						<div class="card-header" role="tab"
							id="heading-<%=categoriesCounter%>">
							<h5 class="mb-0">
								<a data-toggle="collapse" data-parent="#accordion"
									href="#collapse-<%=categoriesCounter%>" aria-expanded="false"
									aria-controls="collapse-<%=categoriesCounter%>">
									${category} </a>
							</h5>
						</div>

						<div id="collapse-<%=categoriesCounter%>" class="collapse"
							role="tabpanel"
							aria-labelledby="heading-<%=categoriesCounter++%>">
							<div class="card-block">
								<table class="table table-bordered">
									<thead class="thead-inverse">
										<tr>
											<th>#</th>
											<th>Item</th>

										</tr>
									</thead>
									<c:forEach var="auditItem" items="${allAuditItens}">
										<c:if test="${auditItem.category == category}">
											<tbody>
												<tr>
													<th scope="row" class="align-middle"><%=questionCounter++%></th>
													<td class="align-middle">${auditItem.question}</td>



												</tr>

											</tbody>
										</c:if>
									</c:forEach>
									<%
										questionCounter = 1;
									%>
								</table>

							</div>
						</div>
					</div>

				</c:forEach>
			</div>
    </div>
    
  </div>
</div>

<jsp:include page="footer.jsp" />
|