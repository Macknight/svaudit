<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<jsp:include page="header.html" />


<div class="row">
	<div class="container ">
		<ul class="nav nav-pills nav-fill">
			<li class="nav-item"><a class="nav-link " href="auditors?command=show">Auditores</a></li>
			<li class="nav-item"><a class="nav-link active" href="audits?command=show">Auditorias</a></li>
			<li class="nav-item"><a class="nav-link" href="itens?command=show">Itens de auditoria</a></li>
			<li class="nav-item"><a class="nav-link " href="reports.jsp">Relat�rios</a>
			</li>
		</ul> 
	</div>
</div>

	
<hr/>
<%int blueprintCounter=1; 
  int questionCounter = 1;
  
%>
<section id="showBlueprints">
<div class="container" >
	<h2>Auditorias:</h2>
	<div id="accordion" role="tablist" aria-multiselectable="false">
		<c:forEach var="blueprint" items="${blueprints}">
			<div class="card">
				<div class="card-header" role="tab"	
					id="heading-<%=blueprintCounter%>">
					<table class="table table-bordered text-justify">
							<tbody>
								<tr>
									<td >
										<h5 class="mb-0" style=" vertical-align: middle;"  >
											<a   data-toggle="collapse" data-parent="#accordion"
												href="#collapse-<%=blueprintCounter%>" aria-expanded="false"
												aria-controls="collapse-<%=blueprintCounter%>">
												${blueprint.name} </a>
										</h5>

									</td>
									<td width="30">
										<form action="audits?command=edit" method="POST">
											<button type="submit" class="btn btn-secondary btn-lg"
												data-toggle="tooltip" data-placement="top" title="Editar">
												<img src="ResoucesShared/ic_mode_edit_black_24px.svg"
													alt="excluir">

											</button>
											<input type="hidden" name="editID" value="${blueprint.id}" />
										</form>
									</td>
							</tbody>
							<tr>
					</table>
					</div>
					
				<div id="collapse-<%=blueprintCounter%>" class="collapse"
					role="tabpanel" aria-labelledby="heading-<%=blueprintCounter++%>">
					<div class="card-block">
						<table class="table table-bordered">
							<thead class="thead-inverse">
								<tr>
									<th>#</th>
									<th>Item</th>
									<th>Categoria</th>
									<th>Obrigatoriedade</th>
								</tr>
							</thead>
							<c:forEach var="auditItem" items="${blueprint.itens}">
									<tbody>
										<tr >
											<th scope="row" class="align-middle"><%=questionCounter++ %></th>
											<td class="align-middle">${auditItem.question}</td>
											<td>
											${auditItem.category}
											</td>
											<c:if test="${auditItem.mandatory eq true}">
												<td>
												<img src="ResoucesShared/ic_check_circle_black_24px.svg" alt="editar" 
												data-toggle="tooltip" 
												data-placement="top" title="Obrigatorio">
												</td>
											</c:if>
												<c:if test="${auditItem.mandatory eq false}">
												<td>
												<img src="ResoucesShared/ic_cancel_black_24px.svg" alt="editar"
												 data-toggle="tooltip" 
												data-placement="top" title="N�o obrigatorio">
												</td>
											</c:if>
											
										</tr>
									</tbody>
							</c:forEach>
							<%questionCounter = 1; %>
						</table>

					</div>
				</div>
			</div>

		</c:forEach>
	</div>
	<form action="audits?command=new" method="POST">
		<button type="submit" class="btn btn-secondary btn-lg btn-block"
			data-toggle="tooltip" data-placement="top" title="Adicionar modelo de auditoria.">
			<img src="ResoucesShared/ic_note_add_white_24px.svg" alt="editar">
		</button>
	</form>
	</div>
</section>



<jsp:include page="footer.jsp" />
