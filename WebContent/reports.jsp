<jsp:include page="header.html" />


<div class="row">
	<div class="container ">
		<ul class="nav nav-pills nav-fill">
			<li class="nav-item"><a class="nav-link " href="auditors?command=show">Auditores</a></li>
			<li class="nav-item"><a class="nav-link " href="audits?command=show">Auditorias</a></li>
			<li class="nav-item" ><a class="nav-link" href="itens?command=show">Itens de auditoria</a></li>
			<li class="nav-item"><a class="nav-link active" href="reports.jsp">Relatórios</a>
			</li>
		</ul>
	</div>
</div>

<hr/>



    <div class="container-fluid">	
      <div class="row ">
      
		
		<!-- report types -->
      
	
        
            <div class="col-6 col-sm-3 placeholder" align="center" >
              <img src="ResoucesShared/Users.png" width="200" height="200" class="img-fluid rounded-circle " alt="Generic placeholder thumbnail">
              <h4 align="center">Usuários</h4>
              <div align="center" class="text-muted"><a  href="reports?command=show&reportObject=user&reportType=summarized">Resumido</a> | <a  href="reports?command=show&reportObject=user&reportType=analytical">Detalhado</a></div>
            </div>
            
            <div class="col-6 col-sm-3 placeholder" align="center" >
              <img src="ResoucesShared/models.png" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
              <h4 align="center">Modelos de Auditorias</h4>
             <div align="center" class="text-muted"><a  href="reports?command=show&reportObject=blueprint&reportType=summarized">Resumido</a> | <a  href="reports?command=show&reportObject=blueprint&reportType=analytical">Detalhado</a></div>
            </div>
            
            <div class="col-6 col-sm-3 placeholder" align="center" >
              <img src="ResoucesShared/doneAudits.png" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
              <h4 align="center">Auditorias</h4>
          <div align="center" class="text-muted"><a  href="reports?command=show&reportObject=auditLog&reportType=summarized">Resumido</a> | <a  href="reports?command=show&reportObject=auditLog&reportType=analytical">Detalhado</a></div>
            </div>
            
            <div class="col-6 col-sm-3 placeholder" align="center" >
              <img src="ResoucesShared/itens.png" width="200" height="200" class="img-fluid rounded-circle" alt="Generic placeholder thumbnail">
              <h4 align="center">Itens de Auditoria.</h4>
                <div align="center" class="text-muted"><a  href="reports?command=show&reportObject=auditItem&reportType=summarized">Resumido</a> | <a  href="reports?command=show&reportObject=auditItem&reportType=analytical">Detalhado</a></div>
            </div>
    
      </div>
    </div>

<jsp:include page="footer.jsp" />

